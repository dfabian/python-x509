import six
from pyasn1.codec.der import decoder
from pyasn1.codec.ber.decoder import stDumpRawValue
from pyasn1.error import PyAsn1Error

import asn1spec
import oids

from x509exceptions import X509DecodeError

decoder.decode.defaultErrorState = stDumpRawValue

class X509AbstractDirectoryName(object):

    SHORT_TAG = None
    OID_NAME = None
    ASN1SPEC = asn1spec.X520Name
    _value = None
    _description = None
    _escape_chars = None

    def __init__(self, value, escape_chars='/+'):
        if not isinstance(value, six.string_types):
            raise TypeError('Expecting string constant')
        try:
            rec = oids.OID_TO_NAME_MAP[self.OID_NAME]
        except KeyError:
            raise ValueError('Invalid OID, possibly a programming error')
        self._value = value
        self._oid = rec[0]
        self._description = rec[1]
        self._escape_chars = escape_chars

    def _escape_value(self):
        ret = self._value
        for c in self._escape_chars:
            ret = ret.replace(c, '\\' + c)
        return ret

    @classmethod
    def _decode_value(cls, asn1):
        """
        Decodes asn1 encoded value into dictionary
        of arguments
        """
        if hasattr(asn1, 'getComponent'):
            value = asn1.getComponent()
        else:
            value = unicode(asn1)
        return {'value' : value}

    def __unicode__(self):
        return u'%s=%s' % (self.SHORT_TAG, self._escape_value())

    def __repr__(self):
        return '<%s: %s>' % (self.__class__.__name__, self._description)

    @classmethod
    def from_asn1(cls, asn1_value):
        assert asn1_value is not None
        try:
            value, _ = decoder.decode(asn1_value, asn1Spec=cls.ASN1SPEC())
            kwargs = cls._decode_value(value)
            return cls(**kwargs)
        except PyAsn1Error:
            raise X509DecodeError('Cannot decode directory name value')


class X520Name(X509AbstractDirectoryName):

    SHORT_TAG = 'name'
    OID_NAME = str(oids.id_at_name)

class X520GivenName(X509AbstractDirectoryName):

    SHORT_TAG = 'givenName'
    OID_NAME = str(oids.id_at_given_name)

class X520Surname(X509AbstractDirectoryName):

    SHORT_TAG = 'SN'
    OID_NAME = str(oids.id_at_surname)

class X520Initials(X509AbstractDirectoryName):

    SHORT_TAG = 'initials'
    OID_NAME = str(oids.id_at_initials)

class X520Title(X509AbstractDirectoryName):

    SHORT_TAG = 'title'
    OID_NAME = str(oids.id_at_title)
    ASN1SPEC = asn1spec.X520Title

class X520DomainNameQualifier(X509AbstractDirectoryName):

    SHORT_TAG = 'dnQualifier'
    OID_NAME = str(oids.id_at_dn_qualifier)
    ASN1SPEC = asn1spec.X520DNQualifier

class X520Country(X509AbstractDirectoryName):

    SHORT_TAG = 'C'
    OID_NAME = str(oids.id_at_country_name)
    ASN1SPEC = asn1spec.X520CountryName

class X520Locality(X509AbstractDirectoryName):

    SHORT_TAG = 'L'
    OID_NAME = str(oids.id_at_locality_name)
    ASN1SPEC = asn1spec.X520LocalityName

class X520StateOrProvince(X509AbstractDirectoryName):

    SHORT_TAG = 'ST'
    OID_NAME = str(oids.id_at_state_or_province)
    ASN1SPEC = asn1spec.X520StateOrProvinceName

class X520Organization(X509AbstractDirectoryName):

    SHORT_TAG = 'O'
    OID_NAME = str(oids.id_at_organization_name)
    ASN1SPEC = asn1spec.X520OrganizationName

class X520OrganizationalUnit(X509AbstractDirectoryName):

    SHORT_TAG = 'OU'
    OID_NAME = str(oids.id_at_organizational_unit_name)
    ASN1SPEC = asn1spec.X520OrganizationalUnitName

class X520CommonName(X509AbstractDirectoryName):

    SHORT_TAG = 'CN'
    OID_NAME = str(oids.id_at_common_name)
    ASN1SPEC = asn1spec.X520CommonName

class X520SerialNumber(X509AbstractDirectoryName):

    SHORT_TAG = 'serialNumber'
    OID_NAME = str(oids.id_at_serial_number)
    ASN1SPEC = asn1spec.X520SerialNumber

class X520Pseudonym(X509AbstractDirectoryName):

    SHORT_TAG = 'pseudonym'
    OID_NAME = str(oids.id_at_pseudonym)
    ASN1SPEC = asn1spec.X520Pseudonym

class X520DomainComponent(X509AbstractDirectoryName):

    SHORT_TAG = 'dnComponent'
    OID_NAME = str(oids.id_domain_component)
    ASN1SPEC = asn1spec.X520DomainComponent

class X520EmailAddress(X509AbstractDirectoryName):

    SHORT_TAG = 'email'
    OID_NAME = str(oids.id_email_address)
    ASN1SPEC = asn1spec.EmailAddress

class X520StreetAddress(X509AbstractDirectoryName):

    SHORT_TAG = 'streetAddress'
    OID_NAME = str(oids.id_at_street_address)
    ASN1SPEC = asn1spec.StreetAddress

    _address_parts = None

    @classmethod
    def _decode_value(cls, asn1):
        """
        Decodes asn1 encoded value into dictionary
        of arguments
        """
        if hasattr(asn1, 'getComponent'):
            value = asn1.getComponent()
        else:
            value = unicode(asn1)
        return {'value' : value}

class X520UID(X509AbstractDirectoryName):

    SHORT_TAG = 'UID'
    OID_NAME = str(oids.id_at_uid)
    ASN1SPEC = asn1spec.X520UID

class X520GenerationQualifier(X509AbstractDirectoryName):

    SHORT_TAG = 'generationQualifier'
    OID_NAME = str(oids.id_at_generation_qualifier)
    ASN1SPEC = asn1spec.X520GenerationQualifier


## EV certificates

class X520BusinessCategory(X509AbstractDirectoryName):

    SHORT_TAG = 'businessCategory'
    OID_NAME = str(oids.id_at_business_category)
    ASN1SPEC = asn1spec.DirectoryString

class X520JurisdictionCountry(X520Country):

    SHORT_TAG = 'jurisdictionCountry'
    OID_NAME = str(oids.id_jurisdiction_of_incorporation_country_name)

class X520JurisdictionStateOrProvince(X520StateOrProvince):

    SHORT_TAG = 'jurisdictionState'
    OID_NAME = str(oids.id_jurisdiction_of_incorporation_state_or_province_name)

class X520JurisdictionLocality(X520Locality):

    SHORT_TAG = 'jurisdictionLocality'
    OID_NAME = str(oids.id_jurisdiction_of_incorporation_locality_name)


class X509RelativeDistinguishedName(object):
    """
    Represents a single relative DN.
    Multi-valued RDNs are also supported.
    """

    _elements = None

    RDN_MAP = {
        X520Name.OID_NAME : X520Name,
        X520GivenName.OID_NAME : X520GivenName,
        X520Surname.OID_NAME : X520Surname,
        X520Initials.OID_NAME : X520Initials,
        X520Title.OID_NAME : X520Title,
        X520DomainNameQualifier.OID_NAME : X520DomainNameQualifier,
        X520Country.OID_NAME : X520Country,
        X520Locality.OID_NAME : X520Locality,
        X520StateOrProvince.OID_NAME : X520StateOrProvince,
        X520Organization.OID_NAME : X520Organization,
        X520OrganizationalUnit.OID_NAME : X520OrganizationalUnit,
        X520CommonName.OID_NAME : X520CommonName,
        X520SerialNumber.OID_NAME : X520SerialNumber,
        X520StreetAddress.OID_NAME : X520StreetAddress,
        X520Pseudonym.OID_NAME : X520Pseudonym,
        X520DomainComponent.OID_NAME : X520DomainComponent,
        X520EmailAddress.OID_NAME : X520EmailAddress,
        X520UID.OID_NAME : X520UID,
        X520BusinessCategory.OID_NAME : X520BusinessCategory,
        X520GenerationQualifier.OID_NAME : X520GenerationQualifier,
        X520JurisdictionCountry.OID_NAME : X520JurisdictionCountry,
        X520JurisdictionStateOrProvince.OID_NAME : X520JurisdictionStateOrProvince,
        X520JurisdictionLocality.OID_NAME : X520JurisdictionLocality,
        }

    def __init__(self, elements):
        if not isinstance(elements, (tuple, list)):
            raise TypeError('Expecting list of RDN elements')
        for element in elements:
            if not isinstance(element, X509AbstractDirectoryName):
                raise TypeError('Expecting DirectoryName instance')
        self._elements = elements

    def __unicode__(self):
        ret = []
        for element in self._elements:
            ret.append(unicode(element))
        return u'+'.join(ret)

    @classmethod
    def from_asn1(cls, asn1):
        assert asn1 is not None
        rdns = []
        for asn1_element in asn1:
            oid = str(asn1_element['type'])
            try:
                rdn_class = cls.RDN_MAP[oid]
                rdns.append(rdn_class.from_asn1(asn1_element['value']))
            except KeyError:
                raise X509DecodeError('Unknown RDN with OID=%s', oid)
        return cls(rdns)


class X509DistinguishedName(object):
    """
    Represents a full DN with multiple RDNS.
    Multi-valued RDNS are also supported
    """

    _delimiter = None
    _rdns = None

    def __init__(self, rdns=None, delimiter='/'):
        self._delimiter = delimiter
        self._rdns = []
        if rdns is not None:
            if not isinstance(elements, (tuple, list)):
                raise TypeError('Expecting list of relative DNs')
            for element in elements:
                if not isinstance(element, X509RelativeDistinguishedName):
                    raise TypeError('Expecting RelativeDistinguishedName instance')
            self._rdns = rdns

    def append_rdn(self, rdn):
        if not isinstance(rdn, X509RelativeDistinguishedName):
            raise TypeError('Expecting RelativeDistinguishedName instance')
        self._rdns.append(rdn)

    def __unicode__(self):
        ret = []
        for rdn in self._rdns:
            ret.append(unicode(rdn))
        return self._delimiter.join(ret)

    @classmethod
    def from_asn1(cls, asn1):
        assert asn1 is not None
        asn1_rdns = asn1['rdnSequence']
        dn = cls()
        for asn1_rdn in asn1_rdns:
            rdn = X509RelativeDistinguishedName.from_asn1(asn1_rdn)
            dn.append_rdn(rdn)
        return dn