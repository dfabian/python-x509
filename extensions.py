import oids
import six
import asn1spec
from collections import OrderedDict
from pyasn1.codec.der import decoder
from pyasn1.codec.ber.decoder import stDumpRawValue
from pyasn1.error import PyAsn1Error

from x509exceptions import X509DecodeError
from dirname import X509DistinguishedName
from generalname import X509GeneralName, X509GeneralNames, X509GeneralNameFactory
from crl import X509CRLDistributionPoints
from serial import X509SerialNumber
from policies import X509PolicyInformation
from crypto_util import pretty_print_binary

class X509CertificateExtension(object):

    OID_NAME = None
    NAME = None
    LABEL = None
    ASN1SPEC = None

    _critical = False

    X509EXTENSIONINFO = {
        #str(oids.id_ce_authority_key_identifier) : ('Authority key identifier', asn1spec.AuthorityKeyIdentifier()),
        #str(oids.id_ce_subject_key_identifier) : ('Subject key identifier', asn1spec.SubjectKeyIdentifier()),
        #str(oids.id_ce_issuer_alt_name) : ('Issuer alternative name', asn1spec.IssuerAltName()),
        #str(oids.id_ce_subject_alt_name) : ('Subject alternative name', asn1spec.SubjectAltName()),
        #str(oids.id_ce_key_usage) : ('Key usage', asn1spec.KeyUsage()),
        str(oids.id_ce_private_key_usage_period) : ('Private key usage period', asn1spec.PrivateKeyUsagePeriod()),
        str(oids.id_ce_certificate_policies) : ('Certificate policies', asn1spec.PolicyInformation()),
        str(oids.id_ce_policy_mappings) : ('Policy mappings', asn1spec.PolicyMapping()),
        str(oids.id_ce_subject_directory_attributes) : ('Subject directory attributes', asn1spec.SubjectDirectoryAttributes()),
        #str(oids.id_ce_name_constraints) : ('Name constraints', asn1spec.NameConstraints()),
        str(oids.id_ce_policy_constraints) : ('Policy constraints', asn1spec.PolicyConstraints()),
        #str(oids.id_ce_ext_key_usage) : ('Extended key usage', asn1spec.ExtKeyUsageSyntax()),
        #str(oids.id_ce_crl_distribution_points) : ('CRL distribution points', asn1spec.CRLDistPointsSyntax()),
        str(oids.id_ce_inhibit_any_policy) : ('Inhibit any policy', asn1spec.InhibitAnyPolicy()),
        str(oids.id_ce_freshest_crl) : ('Freshest CRL', asn1spec.FreshestCRL()),
        #str(oids.id_pe_authority_info_access) : ('Authority info access', asn1spec.AuthorityInfoAccessSyntax()),
        #str(oids.id_pe_subject_info_access) : ('Subject info access', asn1spec.SubjectInfoAccessSyntax()),
        str(oids.id_pe_biometric_info) : ('Biometric info', asn1spec.BiometricSyntax()),
        str(oids.id_pe_qc_statements) : ('Qualified certificate statements', asn1spec.QCStatements()),
        str(oids.id_ms_cert_template) : ('MS certificate template', asn1spec.MSCertExtensionSyntax()),
        str(oids.id_ms_cc_distribution_points) : ('MS cross-certificate distribution points', asn1spec.MSCertExtensionSyntax()),
        str(oids.id_ms_ca_version) : ('MS CA version', asn1spec.MSCertExtensionSyntax()),
        str(oids.id_ms_prev_ca_cert_hash) : ('MS previous CA cert hash', asn1spec.MSCertExtensionSyntax()),
        str(oids.id_ms_cert_template_info) : ('MS certificate template info', asn1spec.MSCertExtensionSyntax()),
        str(oids.id_ms_app_policy) : ('MS application policy', asn1spec.MSCertExtensionSyntax()),
        str(oids.id_ms_app_policy_mapping) : ('MS application policy mapping', asn1spec.MSCertExtensionSyntax()),
        str(oids.id_ms_app_policy_constraints) : ('MS application policy constraints', asn1spec.MSCertExtensionSyntax()),
        }

    def __init__(self, critical=False):
        if not isinstance(critical, bool):
            raise ValueError('Expecting Boolean value')
        self._critical = critical

    def __unicode__(self):
        return u'%s: %s\n\t%s' % (self.LABEL,
                                  'critical' if self._critical else 'not critical',
                                  self._extension_to_string())

    @classmethod
    def _decode_extension(cls, oid, asn1):
        """
        Decodes extension specific data
        """
        raise NotImplementedError

    def _extension_to_string(self):
        """
        Formats extension specific data as unicode
        """
        raise NotImplementedError

    @classmethod
    def from_asn1(cls, oid, asn1):
        assert asn1 is not None
        if cls.OID_NAME:
            assert cls.OID_NAME == oid
        if cls.ASN1SPEC:
            ext_data, _ = decoder.decode(str(asn1['extnValue']), asn1Spec=cls.ASN1SPEC())
        else:
            ext_data = asn1['extnValue']
        kwargs = cls._decode_extension(oid, ext_data)
        extension = cls(bool(asn1['critical']), **kwargs)
        return extension


class X509UnknownExtension(X509CertificateExtension):

    NAME = 'unknown_extension'
    LABEL = 'Unknown extension'

    _oid = None
    _data = None

    def __init__(self, critical, oid, data=None):
        if not isinstance(data, str):
            raise ValueError('Expecting binary data')
        self._oid = oid
        self._data = data

    @classmethod
    def _decode_extension(cls, oid, asn1):
        return {'oid' : oid, 'data' : str(asn1)}

    def _extension_to_string(self):
        return u'OID: %s\nextension value: %s' % (oids.oid_to_name(self._oid), pretty_print_binary(self._data))


class X509BasicConstraintsExtension(X509CertificateExtension):

    OID_NAME = str(oids.id_ce_basic_constraints)
    NAME = 'basic_constraints'
    LABEL = 'Basic constraints'
    ASN1SPEC = asn1spec.BasicConstraints

    _is_ca = False
    _path_length = -1

    def __init__(self, critical=False, is_ca=False, path_length=-1):
        super(X509BasicConstraintsExtension, self).__init__(critical)
        if not isinstance(is_ca, bool):
            raise ValueError('Expecting Boolean value')
        if not isinstance(is_ca, (int, long)):
            raise ValueError('Expecting integer value')
        if not is_ca and path_length != -1:
            raise ValueError('Cannot set path length for non-ca certificate')
        self._is_ca = is_ca
        self._path_length = path_length

    @classmethod
    def _decode_extension(cls, oid, asn1):
        args = dict()
        args['is_ca'] = bool(asn1['cA'])
        path = asn1['pathLenConstraint']
        try:
            if path != None:
                args['path_length'] = int(path)
        except ValueError:
            raise X509DecodeError('Invalid max path length value %s' % path)
        return args

    def _extension_to_string(self):
        path = u''
        if self._path_length >= 0:
            path = u', path length: %u' % self._path_length
        return u'CA: %s%s' % (self._is_ca, path)


class X509AuthorityKeyIdentifierExtension(X509CertificateExtension):

    OID_NAME = str(oids.id_ce_authority_key_identifier)
    NAME = 'authority_key_identifier'
    LABEL = 'Authority key identifier'
    ASN1SPEC = asn1spec.AuthorityKeyIdentifier

    _key_identifier = None
    _issuer = None
    _serial = None

    def __init__(self, critical=False, key_identifier=None, issuer=None, serial=None):
        super(X509AuthorityKeyIdentifierExtension, self).__init__(critical)
        if key_identifier and not isinstance(key_identifier, str):
            raise ValueError('Expecting binary value')
        if issuer and not isinstance(issuer, X509DistinguishedName):
            raise ValueError('Expecting distinguished name')
        if serial and not isinstance(serial, X509SerialNumber):
            raise ValueError('Expecting serial number')
        if not key_identifier and issuer and not serial:
            raise ValueError('Serial number must be set when issuer is set')
        if not key_identifier and not issuer and serial:
            raise ValueError('Issuer name must be set when serial number is set')
        self._key_identifier = key_identifier
        self._issuer = issuer
        self._serial = serial

    @classmethod
    def _decode_extension(cls, oid, asn1):
        args = dict()
        if asn1['keyIdentifier']:
            args['key_identifier'] = str(asn1['keyIdentifier'])
        if asn1['authorityCertIssuer']:
            args['issuer'] = X509DistinguishedName.from_asn1(asn1['authorityCertIssuer'][0]['directoryName'])
        if asn1['authorityCertSerialNumber']:
            args['serial'] = X509SerialNumber.from_asn1(asn1['authorityCertSerialNumber'])
        return args

    def _extension_to_string(self):
        ret = u''
        if self._key_identifier:
            ret += u'keyid: %s\n' % pretty_print_binary(self._key_identifier)
        if self._issuer:
            ret += u'issuer: %s\n' % unicode(self._issuer)
        if self._serial:
            ret += u'serial: %s\n' % unicode(self._serial)
        return ret


class X509SubjectKeyIdentifierExtension(X509CertificateExtension):
    OID_NAME = str(oids.id_ce_subject_key_identifier)
    NAME = 'subject_key_identifier'
    LABEL = 'Subject key identifier'
    ASN1SPEC = asn1spec.SubjectKeyIdentifier

    _key_identifier = None

    def __init__(self, critical=False, key_identifier=None):
        super(X509SubjectKeyIdentifierExtension, self).__init__(critical)
        if key_identifier and not isinstance(key_identifier, str):
            raise ValueError('Expecting binary value')
        if not key_identifier:
            raise ValueError('Key identifier must be set')
        self._key_identifier = key_identifier

    @classmethod
    def _decode_extension(cls, oid, asn1):
        args = dict()
        args['key_identifier'] = str(asn1)
        return args

    def _extension_to_string(self):
        ret = u''
        ret += u'keyid: %s\n' % pretty_print_binary(self._key_identifier)
        return ret


class X509KeyUsageExtension(X509CertificateExtension):
    OID_NAME = str(oids.id_ce_key_usage)
    NAME = 'key_usage'
    LABEL = 'Key usage'
    ASN1SPEC = asn1spec.KeyUsage

    FLAGTOIDXMAP = {
        'digital_signature' : (0, 'Digital signature'),
        'content_commitment' : (1, 'Content commitment'),
        'key_encipherment' : (2, 'Key encipherment'),
        'data_encipherment' : (3, 'Data encipherment'),
        'key_agreement' : (4, 'Key encipherment'),
        'key_cert_sign' : (5, 'Certificate sign'),
        'crl_sign' : (6, 'CRL sign'),
        'encipher_only' : (7, 'Encipher only'),
        'decipher_only' : (8, 'Decipher only'),
        }

    IDXTOFLAGMAP = None

    _usage_flags = None

    def __init__(self, critical=False, flags=None):
        super(X509KeyUsageExtension, self).__init__(critical)
        self.IDXTOFLAGMAP = dict()
        for name, idx in self.FLAGTOIDXMAP.iteritems():
            self.IDXTOFLAGMAP[idx[0]] = (name, idx[1])

        self._usage_flags = [False]*9
        for idx, value in enumerate(flags):
            self._usage_flags[idx] = True if value else False

    def set_usage_flag(self, name, value=False):
        """
        Sets key usage flag name to value value
        """
        try:
            idx = self.FLAGTOIDXMAP[name][0]
            self._usage_flags[idx] = True if value else False
        except KeyError:
            raise ValueError('Invalid key usage name')

    @classmethod
    def _decode_extension(cls, oid, asn1):
        return {'flags' : tuple(asn1)}

    def _extension_to_string(self):
        ret = []
        for i in xrange(0, 9):
            if self._usage_flags[i]:
                ret.append(self.IDXTOFLAGMAP[i][1])
        return ', '.join(ret)


class X509ExtendedKeyUsageExtension(X509CertificateExtension):
    OID_NAME = str(oids.id_ce_ext_key_usage)
    NAME = 'extended_key_usage'
    LABEL = 'Extended key usage'
    ASN1SPEC = asn1spec.ExtKeyUsageSyntax

    _usage_oids = None

    def __init__(self, critical=False, usage_oids=None):
        super(X509ExtendedKeyUsageExtension, self).__init__(critical)
        if not isinstance(usage_oids, (list, tuple)):
            raise ValueError('Expectiong list of usage OIDs')
        for oid in usage_oids:
            if not oids.check_oid(oid):
                raise ValueError('Invalid oid %s' % oid)
        self._usage_oids = list(usage_oids)

    def add_key_usade_oid(self, oid):
        """
        Adds a new oid to the extended usage oids list
        """
        if not oids.check_oid(oid):
            raise ValueError('Invalid oid %s' % oid)
        if oid in self._usage_oids:
            return
        self._usage_oids.append(oid)

    @classmethod
    def _decode_extension(cls, oid, asn1):
        return {'usage_oids' : [str(element) for element in asn1]}

    def _extension_to_string(self):
        ret = list()
        for oid in self._usage_oids:
            ret.append(oids.oid_to_name(oid))
        return ', '.join(ret)


class X509SubjectAlternativeNameExtension(X509CertificateExtension):
    OID_NAME = str(oids.id_ce_subject_alt_name)
    NAME = 'subject_alternative_names'
    LABEL = 'Subject alternative names'
    ASN1SPEC = asn1spec.SubjectAltName

    _alt_names = None

    def __init__(self, critical=False, alternative_names=None):
        super(X509SubjectAlternativeNameExtension, self).__init__(critical)
        if alternative_names and not isinstance(alternative_names, X509GeneralNames):
            raise ValueError('Expecting instance of general names')
        if not alternative_names:
            raise ValueError('Alternative names must be set')
        self._alt_names = alternative_names

    @classmethod
    def _decode_extension(cls, oid, asn1):
        args = dict()
        args['alternative_names'] = X509GeneralNames.from_asn1(asn1)
        return args

    def _extension_to_string(self):
        return unicode(self._alt_names)


class X509IssuerAlternativeNameExtension(X509CertificateExtension):
    OID_NAME = str(oids.id_ce_issuer_alt_name)
    NAME = 'issuer_alternative_names'
    LABEL = 'Issuer alternative names'
    ASN1SPEC = asn1spec.IssuerAltName

    _alt_names = None

    def __init__(self, critical=False, alternative_names=None):
        super(X509IssuerAlternativeNameExtension, self).__init__(critical)
        if alternative_names and not isinstance(alternative_names, X509GeneralNames):
            raise ValueError('Expecting instance of general names')
        if not alternative_names:
            raise ValueError('Alternative names must be set')
        self._alt_names = alternative_names

    @classmethod
    def _decode_extension(cls, oid, asn1):
        args = dict()
        args['alternative_names'] = X509GeneralNames.from_asn1(asn1)
        return args

    def _extension_to_string(self):
        return unicode(self._alt_names)


class X509SingleNameConstraint(object):

    _minimum = 0
    _maximum = -1
    _constraint = None

    def __init__(self, constraint, minimum=0, maximum=-1):
        if not isinstance(constraint, (X509GeneralName, X509DistinguishedName)):
            raise ValueError('Expecting general name instance')
        if maximum >= 0 and minimum > maximum:
            raise ValueError('Minimum value cannot be bigger than maximum')
        self._constraint = constraint
        self._minimum = minimum
        self._maximum = maximum

    def __unicode__(self):
        if self._minimum == 0 and self._maximum == -1:
            return unicode(self._constraint)
        else:
            return unicode(self._constraint) + u'; min: %u, max: %u' % (self._minimum, self._maximum)

    @classmethod
    def from_asn1(cls, asn1):
        minimum = asn1['minimum'] or 0
        maximum = asn1['maximum'] or -1
        constraint = X509GeneralNameFactory.from_asn1(asn1['base'])
        return cls(constraint, minimum, maximum)


class X509NameConstraintsExtension(X509CertificateExtension):
    OID_NAME = str(oids.id_ce_name_constraints)
    NAME = 'name_constraints'
    LABEL = 'Name constraints'
    ASN1SPEC = asn1spec.NameConstraints

    _permitted = None
    _excluded = None

    def __init__(self, critical=False, permitted=None, excluded=None):
        super(X509NameConstraintsExtension, self).__init__(critical)
        if permitted and (not isinstance(permitted, (list, tuple)) or not \
            self._check_constraint_list(permitted)):
            raise ValueError('Expecting list of general names')
        if excluded and (not isinstance(excluded, (list, tuple)) or not\
            self._check_constraint_list(excluded)):
            raise ValueError('Expecting list of general names')
        self._permitted = permitted or list()
        self._excluded = excluded or list()

    def _check_constraint_list(self, constraints):
        for constraint in constraints:
            if not isinstance(constraint, X509SingleNameConstraint):
                return False
        return True

    @classmethod
    def _decode_constraints_list(cls, constraints):
        if not constraints:
            return None
        return [X509SingleNameConstraint.from_asn1(constraint) for constraint in constraints]

    @classmethod
    def _decode_extension(cls, oid, asn1):
        args = dict()
        args['permitted'] = cls._decode_constraints_list(asn1['permittedSubtrees'])
        args['excluded'] = cls._decode_constraints_list(asn1['excludedSubtrees'])
        return args

    def _extension_to_string(self):
        permitted_ret = [unicode(element) for element in self._permitted]
        excluded_ret = [unicode(element) for element in self._excluded]
        return u'permitted:\n%s\nexcluded:\n%s\n' % ('\n'.join(permitted_ret), '\n'.join(excluded_ret))


class X509AccessPoint(object):

    _method = None
    _location = None

    def __init__(self, method, location):
        if not oids.check_oid(method):
            raise ValueError('Invalid access point method')
        if not isinstance(location, (X509GeneralName, X509DistinguishedName)):
            raise ValueError('Expecting general name instance')
        self._method = method
        self._location = location

    @classmethod
    def from_asn1(cls, asn1):
        assert asn1 is not None
        return cls(str(asn1['accessMethod']),
                   X509GeneralNameFactory.from_asn1(asn1['accessLocation']))

    def __unicode__(self):
        try:
            return u'%s: %s' % (oids.OID_TO_NAME_MAP[self._method][1], unicode(self._location))
        except KeyError:
            return u'%s: %s' % (self._method, unicode(self._location))


class X509AuthorityInfoAccessExtension(X509CertificateExtension):
    OID_NAME = str(oids.id_pe_authority_info_access)
    NAME = 'authority_info_access'
    LABEL = 'Authority information access'
    ASN1SPEC = asn1spec.AuthorityInfoAccessSyntax

    _access_points = None

    def __init__(self, critical=False, access_points=None):
        super(X509AuthorityInfoAccessExtension, self).__init__(critical)
        if not access_points:
            raise ValueError('Access points must be set')
        if not self._check_access_list(access_points):
            raise ValueError('Expecting list of access point instances')
        self._access_points = access_points

    def _check_access_list(self, access_points):
        for access_point in access_points:
            if not isinstance(access_point, X509AccessPoint):
                return False
        return True

    @classmethod
    def _decode_extension(cls, oid, asn1):
        args = dict()
        access_list = list()
        for access in asn1:
            access_list.append(X509AccessPoint.from_asn1(access))
        args['access_points'] = access_list
        return args

    def _extension_to_string(self):
        return '\n'.join([unicode(access) for access in self._access_points])


class X509CertificatePoliciesExtension(X509CertificateExtension):
    OID_NAME = str(oids.id_ce_certificate_policies)
    NAME = 'policy_information'
    LABEL = 'Policy information'
    ASN1SPEC = asn1spec.CertificatePolicies

    _policies = None

    def __init__(self, critical=False, policies=None):
        super(X509CertificatePoliciesExtension, self).__init__(critical)
        if not self._check_policy_list(policies):
            raise ValueError('Expecting list of policies instances')
        self._policies = policies or list()

    def _check_policy_list(self, policies):
        for policy in policies:
            if not isinstance(policy, X509PolicyInformation):
                return False
        return True

    @classmethod
    def _decode_extension(cls, oid, asn1):
        args = dict()
        policies_list = list()
        for policy in asn1:
            policies_list.append(X509PolicyInformation.from_asn1(policy))
        args['policies'] = policies_list
        return args

    def _extension_to_string(self):
        return '\n'.join([unicode(policy) for policy in self._policies])


class X509CRLDistributionPointsExtension(X509CertificateExtension):
    OID_NAME = str(oids.id_ce_crl_distribution_points)
    NAME = 'crl_distribution_points'
    LABEL = 'CRL distribution points'
    ASN1SPEC = asn1spec.CRLDistPointsSyntax

    _distribution_points = None

    def __init__(self, critical=False, distribution_points=None):
        super(X509CRLDistributionPointsExtension, self).__init__(critical)
        if not distribution_points:
            raise ValueError('Distribution points must be set')
        if not isinstance(distribution_points, X509CRLDistributionPoints):
            raise ValueError('Expecting distribution points instance')
        self._distribution_points = distribution_points

    @classmethod
    def _decode_extension(cls, oid, asn1):
        args = dict()
        args['distribution_points'] = X509CRLDistributionPoints.from_asn1(asn1)
        return args

    def _extension_to_string(self):
        return unicode(self._distribution_points)


class X509SignedCertificateTimestamp(X509CertificateExtension):

    OID_NAME = str(oids.id_signed_certificate_timestamp)
    NAME = 'signed_certificate_timestamp'
    LABEL = 'Signed certificate timestamp'
    ASN1SPEC = asn1spec.SignedCertificateTimestampList

    _timestamp_list = None

    def __init__(self, critical=False, timestamp_list=None):
        super(X509SignedCertificateTimestamp, self).__init__(critical)
        if not timestamp_list:
            raise ValueError('Timestamp list must be set')
        if not isinstance(timestamp_list, str):
            raise ValueError('Expecting binary data')
        self._timestamp_list = timestamp_list

    @classmethod
    def _decode_extension(cls, oid, asn1):
        args = dict()
        args['timestamp_list'] = str(asn1)
        return args

    def _extension_to_string(self):
        return u'timestamp list:\n%s' % pretty_print_binary(self._timestamp_list, offset=len('timestamp list:'))



class X509CertificateExtensions(object):

    _extensions = None

    X509EXTENSIONSMAP = {
        X509BasicConstraintsExtension.OID_NAME : X509BasicConstraintsExtension,
        X509AuthorityKeyIdentifierExtension.OID_NAME : X509AuthorityKeyIdentifierExtension,
        X509SubjectKeyIdentifierExtension.OID_NAME : X509SubjectKeyIdentifierExtension,
        X509KeyUsageExtension.OID_NAME : X509KeyUsageExtension,
        X509ExtendedKeyUsageExtension.OID_NAME : X509ExtendedKeyUsageExtension,
        X509SubjectAlternativeNameExtension.OID_NAME : X509SubjectAlternativeNameExtension,
        X509NameConstraintsExtension.OID_NAME : X509NameConstraintsExtension,
        X509AuthorityInfoAccessExtension.OID_NAME : X509AuthorityInfoAccessExtension,
        X509CRLDistributionPointsExtension.OID_NAME : X509CRLDistributionPointsExtension,
        X509CertificatePoliciesExtension.OID_NAME : X509CertificatePoliciesExtension,
        X509SignedCertificateTimestamp.OID_NAME : X509SignedCertificateTimestamp,
        }

    def __init__(self):
        self._extensions = OrderedDict()

    def append_extension(self, extension):
        if not isinstance(extension, X509CertificateExtension):
            raise TypeError('Expecting cert extension instance')
        if not extension.NAME in self._extensions:
            self._extensions[extension.NAME] = list()
        self._extensions[extension.NAME].append(extension)

    def __unicode__(self):
        ext_str = []
        for name, exts in self._extensions.iteritems():
            ext_str += [unicode(ext) for ext in exts]
        return '\n'.join(ext_str)

    @classmethod
    def from_asn1(cls, asn1):
        assert asn1 is not None
        extensions = X509CertificateExtensions()
        for ext_asn1 in asn1:
            oid = str(ext_asn1['extnID'])
            try:
                extension_class = X509CertificateExtensions.X509EXTENSIONSMAP[oid]
                extensions.append_extension(extension_class.from_asn1(oid, ext_asn1))
            except KeyError:
                extensions.append_extension(X509UnknownExtension.from_asn1(oid, ext_asn1))
        return extensions



