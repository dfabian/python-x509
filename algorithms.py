import oids
import six
import asn1spec
from pyasn1.codec.der import decoder
from pyasn1.codec.ber.decoder import stDumpRawValue
from pyasn1.error import PyAsn1Error

from x509exceptions import X509DecodeError
import crypto_util

class X509AbstractAlgorithmInfo(object):

    OID_NAME = None
    HASH = None
    ENCRYPTION = None
    ASN1SPEC = asn1spec.NullParameters
    _description = None
    _parameters = None

    def __init__(self, parameters=None):
        if parameters:
            if not isinstance(parameters, (tuple, list)):
                raise TypeError('Expecting list of parameters')
        try:
            rec = oids.NAME_TO_OID_MAP[self.OID_NAME]
        except KeyError:
            raise ValueError('Invalid OID, possibly a programming error')
        self._oid = rec[0]
        self._description = rec[1]
        self._parameters = parameters or []

    def __unicode__(self):
        return unicode(oids.NAME_TO_OID_MAP[self.OID_NAME][1])

    def __repr__(self):
        return '<%s: %s>' % (self.__class__.__name__, self._description)

    def __eq__(self, other):
        return self.__class__.__name__ == other.__class__.__name__

    def __ne__(self, other):
        return not self.__eq__(other)

    def get_encryption_algorithm(self):
        return self.ENCRYPTION

    def get_hash_algorithm(self):
        return self.HASH

    def get_parameters(self):
        return self._parameters

    def is_encryption_algorithm(self):
        return self.ENCRYPTION != ''

    def is_hash_algorithm(self):
        return self.HASH != ''

    @classmethod
    def from_asn1(cls, asn1_params):
        assert asn1_params is not None
        try:
            parameters, _ = decoder.decode(asn1_params, asn1Spec=cls.ASN1SPEC())
            return cls(list(parameters))
        except PyAsn1Error:
            raise X509DecodeError('Cannot decode algorithm parameters')


class X509MD2AlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_md2'
    HASH = 'md2'

class X509MD5AlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_md5'
    HASH = 'md5'

class X509SHA1AlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha1'
    HASH = 'sha1'

class X509SHA224AlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha224'
    HASH = 'sha224'

class X509SHA256AlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha256'
    HASH = 'sha256'

class X509SHA384AlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha384'
    HASH = 'sha384'

class X509SHA512AlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha512'
    HASH = 'sha512'

class X509RSAAlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_rsa'
    HASH = ''
    ENCRYPTION = 'rsa'

class X509SHA1WithRSAAlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha1_with_rsa'
    HASH = 'sha1'
    ENCRYPTION = 'rsa'

class X509SHA224WithRSAAlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha224_with_rsa'
    HASH = 'sha224'
    ENCRYPTION = 'rsa'

class X509SHA256WithRSAAlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha256_with_rsa'
    HASH = 'sha256'
    ENCRYPTION = 'rsa'

class X509SHA384WithRSAAlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha384_with_rsa'
    HASH = 'sha384'
    ENCRYPTION = 'rsa'

class X509SHA512WithRSAAlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha512_with_rsa'
    HASH = 'sha512'
    ENCRYPTION = 'rsa'

class X509DSAAlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_dsa'
    HASH = ''
    ENCRYPTION = 'dsa'

class X509SHA1WithDSAAlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha1_with_dsa'
    HASH = 'sha1'
    ENCRYPTION = 'dsa'

class X509DHAlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_dhpublic_number'
    HASH = ''
    ENCRYPTION = 'dhpublic_number'

class X509KEAAlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_key_exchange'
    HASH = ''
    ENCRYPTION = 'kea'

class X509SHA1WithECDSAAlgorithmInfo(X509AbstractAlgorithmInfo):

    OID_NAME = 'id_sha1_with_ecdsa'
    HASH = 'sha1'
    ENCRYPTION = 'edca'

    #X509ALGOIDS = {
        #'1.2.840.113549.1.1.7' : ('','rsaes-oaep', rfc4055.RSAESOAEPParameters),
        #'1.2.840.113549.1.1.10' : ('','rsassa-pss', rfc4055.RSASSAPSSParameters),
        #}


class X509Algorithm(object):

    _info = None

    ALG_IMPL_MAP = {
        X509MD2AlgorithmInfo.OID_NAME : X509MD2AlgorithmInfo,
        X509MD5AlgorithmInfo.OID_NAME : X509MD5AlgorithmInfo,
        X509SHA1AlgorithmInfo.OID_NAME : X509SHA1AlgorithmInfo,
        X509SHA224AlgorithmInfo.OID_NAME : X509SHA224AlgorithmInfo,
        X509SHA256AlgorithmInfo.OID_NAME : X509SHA256AlgorithmInfo,
        X509SHA384AlgorithmInfo.OID_NAME : X509SHA384AlgorithmInfo,
        X509SHA512AlgorithmInfo.OID_NAME : X509SHA512AlgorithmInfo,
        X509RSAAlgorithmInfo.OID_NAME : X509RSAAlgorithmInfo,
        X509SHA1WithRSAAlgorithmInfo.OID_NAME : X509SHA1WithRSAAlgorithmInfo,
        X509SHA224WithRSAAlgorithmInfo.OID_NAME : X509SHA224WithRSAAlgorithmInfo,
        X509SHA256WithRSAAlgorithmInfo.OID_NAME : X509SHA256WithRSAAlgorithmInfo,
        X509SHA384WithRSAAlgorithmInfo.OID_NAME : X509SHA384WithRSAAlgorithmInfo,
        X509SHA512WithRSAAlgorithmInfo.OID_NAME : X509SHA512WithRSAAlgorithmInfo,
        X509DSAAlgorithmInfo.OID_NAME : X509DSAAlgorithmInfo,
        X509SHA1WithDSAAlgorithmInfo.OID_NAME : X509SHA1WithDSAAlgorithmInfo,
        X509DHAlgorithmInfo.OID_NAME : X509DHAlgorithmInfo,
        X509KEAAlgorithmInfo.OID_NAME : X509KEAAlgorithmInfo,
        X509SHA1WithECDSAAlgorithmInfo.OID_NAME : X509SHA1WithECDSAAlgorithmInfo,
        }

    def __init__(self, info):
        if not isinstance(info, X509AbstractAlgorithmInfo):
            raise TypeError('Expecting algorithm info instance')
        self._info = info

    def __unicode__(self):
        return unicode(self._info)

    def __eq__(self, other):
        return self._info == other._info

    def __ne__(self, other):
        return not self.__eq__(other)

    def get_algorithm_info(self):
        return self._info

    @classmethod
    def from_asn1(cls, asn1):
        assert asn1 is not None
        oid = str(asn1['algorithm'])
        try:
            oid_name = oids.OID_TO_NAME_MAP[oid][0]
            rdn_class = cls.ALG_IMPL_MAP[oid_name]
            return X509Algorithm(rdn_class.from_asn1(asn1['parameters']))
        except KeyError:
            raise X509DecodeError('Unknown algorithm with OID=%s', oid)


class X509Signature(object):

    _sig_alg = None
    _signature = None

    def __init__(self, sig_alg, signature):
        if not isinstance(sig_alg, X509Algorithm):
            raise TypeError('Expecting X509Algorithm isntance')
        if not isinstance(signature, (bytearray, six.binary_type)):
            raise TypeError('Expecting bytes')
        self._sig_alg = sig_alg
        self._signature = signature

    def __unicode__(self):
        return unicode(self._sig_alg) + '\n' +\
               crypto_util.pretty_print_binary(self._signature, offset=9)

    @classmethod
    def from_asn1(cls, alg_asn1, sig_asn1):
        alg = X509Algorithm.from_asn1(alg_asn1)
        signature = cls(alg, str(crypto_util.bits_to_bytearray(tuple(sig_asn1))))
        return signature

    def get_algorithm(self):
        return self._sig_alg



