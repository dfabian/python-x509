import oids
import asn1spec
from pyasn1.codec.der import decoder
from pyasn1.codec.ber.decoder import stDumpRawValue
from pyasn1.error import PyAsn1Error

from x509exceptions import X509DecodeError


class X509PolicyQualifier(object):

    OID_NAME = None
    NAME = None
    LABEL = None
    ASN1SPEC = None

    @classmethod
    def _decode_qualifier_data(cls, asn1):
        """
        Decodes qualifier specific data. Must be implemented in subclass
        """
        raise NotImplementedError

    def _qualifier_data_to_string(self):
        """
        Formats qualifier specific data as string. Must be implemented in subclass
        """
        raise NotImplementedError

    @classmethod
    def from_asn1(cls, asn1):
        assert asn1 is not None
        if str(asn1['policyQualifierId']) != cls.OID_NAME:
            raise ValueError('Invalid qualifier ID')
        try:
            value, _ = decoder.decode(str(asn1['qualifier']), asn1Spec=cls.ASN1SPEC())
            kwargs = cls._decode_qualifier_data(value)
            return cls(**kwargs)
        except PyAsn1Error:
            raise X509DecodeError('Cannot decode general name value')

    def __unicode__(self):
        return "%s: %s" % (self.LABEL,
                           self._qualifier_data_to_string())


class X509CertificatePolicyPointer(X509PolicyQualifier):

    OID_NAME = str(oids.id_qt_cps)
    NAME = 'certificate-policy-pointer'
    LABEL = 'Policy pointer'
    ASN1SPEC = asn1spec.CPSuri

    _uri = None

    def __init__(self, uri=None):
        if not uri:
            raise ValueError('URI must be set')
        self._uri = str(uri)

    @classmethod
    def _decode_qualifier_data(cls, asn1):
        args = dict()
        args['uri'] = str(asn1)
        return args

    def _qualifier_data_to_string(self):
        """
        Formats qualifier specific data as string. Must be implemented in subclass
        """
        return u'URI: %s' % self._uri


class X509UserNotice(object):

    _notice_ref_text = None
    _notice_ref_numbers = None
    _explicit_text = None

    def __init__(self, notice_ref_text=None, notice_ref_numbers=None, explicit_text=None):
        if notice_ref_text and not notice_ref_numbers or\
           notice_ref_numbers and not notice_ref_text:
               raise ValueError('Both notice reference text and reference numbers must be set')
        if not notice_ref_text and not notice_ref_numbers and not explicit_text:
            raise ValueError('At least one user notice must be set')
        if notice_ref_text:
            if not isinstance(notice_ref_numbers, (tuple, list)):
                raise ValueError('Expecting list of reference numbers')
            for number in notice_ref_numbers:
                if not isinstance(number, (int, long)):
                    raise ValueError('Expecting list of reference numbers')
        self._notice_ref_text = notice_ref_text
        self._notice_ref_numbers = notice_ref_numbers or list()
        self._explicit_text = explicit_text

    def __unicode__(self):
        if self._notice_ref_text:
            reference_str = 'organization: %s, numbers: %s' % (self._notice_ref_text, ', '.join(self._notice_ref_numbers))
        else:
            reference_str = None
        return '; '.join(filter(None, [reference_str, 'explicit text %s' % self._explicit_text if self._explicit_text else None]))


class X509UserNoticeQualifier(X509PolicyQualifier):

    OID_NAME = str(oids.id_qt_unotice)
    NAME = 'certificate-user-notice'
    LABEL = 'User notice'
    ASN1SPEC = asn1spec.UserNotice

    _notice_refs = None

    def __init__(self, notice_references=None):
        self._check_notice_references(notice_references)
        self._notice_refs = notice_references

    def _check_notice_references(self, references):
        if not references:
            return
        for reference in references:
            if not isinstance(reference, X509UserNotice):
                raise ValueError('Expecting list of user notice instances')

    @classmethod
    def _decode_qualifier_data(cls, asn1):
        args = dict()
        explicit_texts = list()
        refs = list()
        notice = asn1
        explicit_text = unicode(notice['explicitText'].getComponent())
        if notice['noticeRef']:
            refs.append(X509UserNotice(unicode(notice['noticeRef']['organization'].getComponent()),
                                        notice['noticeRef']['noticeNumbers'],
                                        explicit_text))
        else:
            refs.append(X509UserNotice(explicit_text=explicit_text))

        args['notice_references'] = refs
        return args

    def _qualifier_data_to_string(self):
        return '\n'.join([unicode(ref) for ref in self._notice_refs])


class X509PolicyInformation(object):

    _policy_oid = None
    _qualifiers = None

    QUALIFIER_MAP = {
        X509CertificatePolicyPointer.OID_NAME : X509CertificatePolicyPointer,
        X509UserNoticeQualifier.OID_NAME : X509UserNoticeQualifier,
        }

    def __init__(self, oid, qualifiers=None):
        if not oids.check_oid(oid):
            raise ValueError('Invalid object identifier')
        self._check_qualifiers(qualifiers)
        self._policy_oid = oid
        self._qualifiers = qualifiers or list()

    def _check_qualifiers(self, qualifiers):
        if not qualifiers:
            return
        for qualifier in qualifiers:
            if not isinstance(qualifier, X509PolicyQualifier):
                raise ValueError('Expecting policy qualifier instance')

    def add_qualifier(self, qualifier):
        if not isinstance(qualifier, X509PolicyQualifier):
            raise ValueError('Expecting policy qualifier instance')
        self._qualifiers.append(qualifier)

    @classmethod
    def from_asn1(cls, asn1):
        qualifiers = list()
        if asn1['policyQualifiers']:
            for qualifier in asn1['policyQualifiers']:
                try:
                    qualifier_class = cls.QUALIFIER_MAP[str(qualifier['policyQualifierId'])]
                    qualifiers.append(qualifier_class.from_asn1(qualifier))
                except KeyError:
                    raise X509DecodeError('Unknown policy qualifier ID %s' % str(qualifier['policyQualifierId']))
        return cls(str(asn1['policyIdentifier']), qualifiers)

    def __unicode__(self):
        return u'policy ID: %s, %s' % (oids.oid_to_name(self._policy_oid),
                                       '\n'.join([unicode(qualifier) for qualifier in self._qualifiers]))


