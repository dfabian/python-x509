from pyasn1.type import univ
from pyasn1.error import PyAsn1Error

OID_TO_NAME_MAP = {
    '1.3.6.1.5.5.7'       : ('id_pkix', 'PKIX ID'),
    '1.3.6.1.5.5.7.2'     : ('id_qt', 'QT ID'),
    '1.3.6.1.5.5.7.1'     : ('id_pe' 'PE ID'),
    '1.3.6.1.5.5.7.3'     : ('id_kp', 'KP ID'),
    '1.3.6.1.5.5.7.48'    : ('id_ad', 'AD ID'),
    '2.5.4'               : ('id_at', 'AT ID'),
    '2.5.29'              : ('id_ce', 'CE ID'),
    '1.3.6.1.5.5.7.2.1'   : ('id_qt_cps', 'CPS QT ID'),
    '1.3.6.1.5.5.7.2.2'   : ('id_qt_unotice', 'Unotice QT ID'),
    '1.3.6.1.5.5.7.48.1'  : ('id_ad_ocsp', 'OCSP record'),
    '1.3.6.1.5.5.7.48.2'  : ('id_ad_ca_issuers', 'CA issuers'),
    '1.3.6.1.5.5.7.48.3'  : ('id_ad_time_stamping', 'Time stamping'),
    '1.3.6.1.5.5.7.48.5'  : ('id_ad_ca_repository', 'CA repository'),
    '2.5.4.41'            : ('id_at_name', 'X520 Name'),
    '2.5.4.4'             : ('id_at_surname', 'Surname'),
    '2.5.4.42'            : ('id_at_given_name', 'Given name'),
    '2.5.4.43'            : ('id_at_initials', 'Initials'),
    '2.5.4.44'            : ('id_at_generation_qualifier', 'Generation qualifier'),
    '2.5.4.3'             : ('id_at_common_name', 'Common name'),
    '2.5.4.7'             : ('id_at_locality_name', 'Locality'),
    '2.5.4.8'             : ('id_at_state_or_province', 'State or province'),
    '2.5.4.9'             : ('id_at_street_address', 'Street address'),
    '2.5.4.10'            : ('id_at_organization_name', 'Organization'),
    '2.5.4.11'            : ('id_at_organizational_unit_name', 'Organizational unit'),
    '2.5.4.12'            : ('id_at_title', 'Title'),
    '2.5.4.15'            : ('id_at_business_category', 'Business category'),
    '2.5.4.17'            : ('id_at_postal_code', 'Postal code'),
    '2.5.4.46'            : ('id_at_dn_qualifier', 'DN qualifier'),
    '2.5.4.6'             : ('id_at_country_name', 'Country'),
    '2.5.4.5'             : ('id_at_serial_number', 'Serial number'),
    '2.5.4.65'            : ('id_at_pseudonym', 'Pseudonym'),
    '0.9.2342.19200300.100.1.1' : ('id_at_uid', 'Unique ID'),
    '0.9.2342.19200300.100.1.25' : ('id_domain_component', 'Domain component'),
    '1.2.840.113549.1.9'  : ('id_pkcs_9', 'PKCS#9'),
    '1.2.840.113549.1.9.1' : ('id_email_address', 'Email'),
    '1.2.840.113549.1.1'  : ('id_pkcs_1', 'PKCS#1'),
    '1.2.840.113549.1.1.1' : ('id_rsa', 'RSA encryption'),
    '1.2.840.113549.1.1.2' : ('id_md2_with_rsa', 'MD2 with RSA encryption'),
    '1.2.840.113549.1.1.4' : ('id_md5_with_rsa', 'MD5 with RSA encryption'),
    '1.2.840.113549.1.1.5' : ('id_sha1_with_rsa', 'SHA1 with RSA encryption'),
    '1.2.840.10040.4.3'   : ('id_sha1_with_dsa', 'SHA1 with DSA encryption'),
    '1.2.840.10046.2.1'   : ('id_dhpublic_number', 'Diffie-Hellman number'),
    '1.2.840.10040.4.1'   : ('id_dsa', 'DSA encryption'),
    '2.16.840.1.101.2.1.1.22' : ('id_key_exchange', 'Key exchange'),
    '1.2.840.10045.4.1'   : ('id_sha1_with_ecdsa', 'SHA1 with ECDSA encryption'),
    '1.3.14.7.2.2.1'      : ('id_md2', 'MD2'),
    '1.3.14.7.2.2.5'      : ('id_md5', 'MD5'),
    '1.3.14.3.2.26'       : ('id_sha1', 'SHA1'),
    '2.16.840.1.101.3.4.2.4' : ('id_sha224', 'SHA224'),
    '2.16.840.1.101.3.4.2.1' : ('id_sha256', 'SHA256'),
    '2.16.840.1.101.3.4.2.2' : ('id_sha384', 'SHA384'),
    '2.16.840.1.101.3.4.2.3' : ('id_sha512', 'SHA512'),
    '1.2.840.113549.1.1.14'       : ('id_sha224_with_rsa', 'SHA224 with RSA'),
    '1.2.840.113549.1.1.11'       : ('id_sha256_with_rsa', 'SHA256 with RSA'),
    '1.2.840.113549.1.1.12'       : ('id_sha384_with_rsa', 'SHA384 with RSA'),
    '1.2.840.113549.1.1.13'       : ('id_sha512_with_rsa', 'SHA512 with RSA'),
    '2.5.29.24'           : ('id_ce_invalidity_date', 'Invalidity date extension'),
    '2.2.840.10040.2.1'   : ('id_holdinstruction_none', 'None (hold instruction)'),
    '2.2.840.10040.2.2'   : ('id_holdinstruction_callissuer', 'Call issuer'),
    '2.2.840.10040.2.3'   : ('id_holdinstruction_reject', 'Reject (hold instruction)'),
    '2.5.29.23'           : ('id_holdinstruction_code', 'Hold instruction code'),
    '2.2.840.10040.2'     : ('id_holdinstruction', 'Hold instruction'),
    '2.5.29.21'           : ('id_ce_crl_reasons', 'CRL reasons'),
    '2.5.29.20'           : ('id_ce_crl_number', 'CRL number'),
    '1.3.6.1.5.5.7.3.1'   : ('id_kp_server_auth', 'Server authentication'),
    '1.3.6.1.5.5.7.3.2'   : ('id_kp_client_auth', 'Client authentication'),
    '1.3.6.1.5.5.7.3.3'   : ('id_kp_code_signing', 'Code signing'),
    '1.3.6.1.5.5.7.3.4'   : ('id_kp_email_protection', 'Email protection'),
    '1.3.6.1.5.5.7.3.5'   : ('id_kp_ipsec_end_system', 'IPSec end system'),
    '1.3.6.1.5.5.7.3.6'   : ('id_kp_ipsec_tunnel', 'IPSec tunnel'),
    '1.3.6.1.5.5.7.3.7'   : ('id_kp_ipsec_user', 'IPSec user'),
    '1.3.6.1.5.5.7.3.8'   : ('id_kp_time_stamping', 'Time stamping'),
    '2.5.29.37'           : ('id_ce_ext_key_usage', 'Extended key usage'),
    '1.3.6.1.5.5.7.1.1'   : ('id_pe_authority_info_access', 'Authority information access'),
    '1.3.6.1.5.5.7.1.11'  : ('id_pe_subject_info_access', 'Subject information access'),
    '1.3.6.1.5.5.7.1.12'  : ('id_pe_logotype', 'Logo type'),
    '1.3.6.1.5.5.7.1.2'   : ('id_pe_biometric_info', 'Biometric information'),
    '1.3.6.1.5.5.7.1.3'   : ('id_pe_qc_statements', 'Qualified certificate statements'),
    '2.5.29.32.0'         : ('id_ce_any_policy', 'Any policy'),
    '2.5.29.36'           : ('id_ce_policy_constraints', 'Policy constraints'),
    '2.5.29.54'           : ('id_ce_inhibit_any_policy', 'Inhibit any policy'),
    '2.5.29.19'           : ('id_ce_basic_constraints', 'Basic constraints'),
    '2.5.29.9'            : ('id_ce_subject_directory_attributes', 'Subject directory attributes'),
    '2.5.29.27'           : ('id_ce_delta_crl_indicator', 'Delta CRL indicator'),
    '2.5.29.31'           : ('id_ce_crl_distribution_points', 'CRL distribution points'),
    '2.5.29.46'           : ('id_ce_freshest_crl', 'Freshest CRL'),
    '2.5.29.28'           : ('id_ce_issuing_distribution_point', 'Issuing distribution point'),
    '2.5.29.30'           : ('id_ce_name_constraints', 'Name constraints'),
    '2.5.29.32'           : ('id_ce_certificate_policies', 'Certificate policies'),
    '2.5.29.33'           : ('id_ce_policy_mappings', 'Policy mappings'),
    '2.5.29.16'           : ('id_ce_private_key_usage_period', 'Private key usage period'),
    '2.5.29.15'           : ('id_ce_key_usage', 'Key usage'),
    '2.5.29.35'           : ('id_ce_authority_key_identifier', 'Authority key identifier'),
    '2.5.29.14'           : ('id_ce_subject_key_identifier', 'Subject key identifier'),
    '2.5.29.29'           : ('id_ce_certificate_issuer', 'Certificate issuer'),
    '2.5.29.17'           : ('id_ce_subject_alt_name', 'Subject alternative names'),
    '2.5.29.18'           : ('id_ce_issuer_alt_name', 'Issuer alternative name'),
    '1.3.14.3.2.8'        : ('id_mgf1', 'Mask defining function 1'),
    '1.3.14.3.2.9'        : ('id_p_specified', 'P specified'), 

    '1.3.6.1.4.1.311.20.2' : ('id_ms_cert_template', 'MS certificate template'),
    '1.3.6.1.4.1.311.10.9.1' : ('id_ms_cc_distribution_points', 'MS cross-certificate distribution points'),
    '1.3.6.1.4.1.311.21.1' : ('id_ms_ca_version', 'MS CA version'),
    '1.3.6.1.4.1.311.21.2' : ('id_ms_prev_ca_cert_hash', 'MS previous CA certificate hash'),
    '1.3.6.1.4.1.311.21.7' : ('id_ms_cert_template_info', 'MS certificate template info'),
    '1.3.6.1.4.1.311.21.10' : ('id_ms_app_policy', 'MS application policy'),
    '1.3.6.1.4.1.311.21.11' : ('id_ms_app_policy_mapping', 'MS application policy mapping'),
    '1.3.6.1.4.1.311.21.12' : ('id_ms_app_policy_constraints', 'MS application policy constraints'),

    '2.16.840.1.113730.4.1' : ('id_ns_server_gated_crypto', 'Netscape Server Gated Crypto'),

    '1.3.6.1.4.1.311.60.2.1.1' : ('id_jurisdiction_of_incorporation_locality_name', 'Jurisdiction of incorporation locality name'),
    '1.3.6.1.4.1.311.60.2.1.2' : ('id_jurisdiction_of_incorporation_state_or_province_name', 'Jurisdiction of incorporation state or province name'),
    '1.3.6.1.4.1.311.60.2.1.3' : ('id_jurisdiction_of_incorporation_country_name', 'Jurisdiction of incorporation country name'),

    '1.3.6.1.4.1.11129.2.4.2' : ('id_signed_certificate_timestamp', 'Signed certificate timestamp'),
    '1.3.6.1.4.1.11129.2.4.4' : ('id_certificate_transparency', 'Certificate transparency'),
    }

NAME_TO_OID_MAP = {}
for oid, rec in OID_TO_NAME_MAP.iteritems():
    NAME_TO_OID_MAP[rec[0]] = (oid, rec[1])
    globals()[rec[0]] = univ.ObjectIdentifier(oid)


def check_oid(oid):
    """
    Checks the validity of the oid.
    Returns True, if oid is valid, False otherwise.
    """
    try:
        univ.ObjectIdentifier(oid)
        return True
    except PyAsn1Error:
        return False


def oid_to_name(oid):
    try:
        return OID_TO_NAME_MAP[oid][1]
    except KeyError:
        return oid


#id_rsassa_pss = univ.ObjectIdentifier('1.3.14.3.2.10')

#id_rsaes_oaep =  univ.ObjectIdentifier('1.2.840.113549.1.1.7')

#id_ecSigType = univ.ObjectIdentifier('1.2.840.10045.4')

#id_fieldType = univ.ObjectIdentifier('1.2.840.10045.1')

#id_prime_field = univ.ObjectIdentifier('1.2.840.10045.1.1')

#id_characteristic_two_field = univ.ObjectIdentifier('1.2.840.10045.1.2')

#id_characteristic_two_basis = univ.ObjectIdentifier('1.2.840.10045.1.2.3')

#id_gnBasis = univ.ObjectIdentifier('1.2.840.10045.1.2.3.1')

#id_tpBasis = univ.ObjectIdentifier('1.2.840.10045.1.2.3.2')

#id_ppBasis = univ.ObjectIdentifier('1.2.840.10045.1.2.3.3')

#id_publicKeyType = univ.ObjectIdentifier('1.2.840.10045.2')

#id_ecPublicKey = univ.ObjectIdentifier('1.2.840.10045.2.1')

#id_ellipticCurve = univ.ObjectIdentifier('1.2.840.10045.3')

#id_c_twocurve = univ.ObjectIdentifier('1.2.840.10045.3.0')

#id_c2pnb163v1 = univ.ObjectIdentifier('1.2.840.10045.3.1')
#id_c2pnb163v2 = univ.ObjectIdentifier('1.2.840.10045.3.2')
#id_c2pnb163v3 = univ.ObjectIdentifier('1.2.840.10045.3.3')
#id_c2pnb176w1 = univ.ObjectIdentifier('1.2.840.10045.3.4')
#id_c2tnb191v1 = univ.ObjectIdentifier('1.2.840.10045.3.5')
#id_c2tnb191v2 = univ.ObjectIdentifier('1.2.840.10045.3.6')
#id_c2tnb191v3 = univ.ObjectIdentifier('1.2.840.10045.3.7')
#id_c2tnb191v4 = univ.ObjectIdentifier('1.2.840.10045.3.8')
#id_c2onb191v5 = univ.ObjectIdentifier('1.2.840.10045.3.9')
#id_c2pnb208w1 = univ.ObjectIdentifier('1.2.840.10045.3.10')
#id_c2tnb239v1 = univ.ObjectIdentifier('1.2.840.10045.3.11')
#id_c2tnb239v2 = univ.ObjectIdentifier('1.2.840.10045.3.12')
#id_c2tnb239v3 = univ.ObjectIdentifier('1.2.840.10045.3.13')
#id_c2onb239v4 = univ.ObjectIdentifier('1.2.840.10045.3.14')
#id_c2onb239v5 = univ.ObjectIdentifier('1.2.840.10045.3.15')
#id_c2pnb272w1 = univ.ObjectIdentifier('1.2.840.10045.3.16')
#id_c2pnb304w1 = univ.ObjectIdentifier('1.2.840.10045.3.17')
#id_c2tnb359v1 = univ.ObjectIdentifier('1.2.840.10045.3.18')
#id_c2pnb368w1 = univ.ObjectIdentifier('1.2.840.10045.3.19')
#id_c2tnb431r1 = univ.ObjectIdentifier('1.2.840.10045.3.20')

#id_primeCurve = univ.ObjectIdentifier('1.2.840.10045.3.1')

#id_prime192v1 = univ.ObjectIdentifier('1.2.840.10045.3.1.1')
#id_prime192v2 = univ.ObjectIdentifier('1.2.840.10045.3.1.2')
#id_prime192v3 = univ.ObjectIdentifier('1.2.840.10045.3.1.3')
#id_prime239v1 = univ.ObjectIdentifier('1.2.840.10045.3.1.4')
#id_prime239v2 = univ.ObjectIdentifier('1.2.840.10045.3.1.5')
#id_prime239v3 = univ.ObjectIdentifier('1.2.840.10045.3.1.6')
#id_prime256v1 = univ.ObjectIdentifier('1.2.840.10045.3.1.7')
