import oids
import asn1spec

from dirname import X509DistinguishedName
from pyasn1.codec.der import decoder
from pyasn1.codec.ber.decoder import stDumpRawValue
from pyasn1.error import PyAsn1Error

from email.utils import parseaddr
from IPy import IP

from x509exceptions import X509DecodeError
from crypto_util import pretty_print_binary

class X509GeneralName(object):

    ASN1SPEC = None

    def _validate_data(self, data):
        """
        Validates general name data. Must be implemented in subclass.
        """
        raise NotImplementedError

    @classmethod
    def _decode_value(cls, asn1):
        """
        Decodes general name specific value.  Must be implemented in subclass.
        """
        raise NotImplementedError

    @classmethod
    def from_asn1(cls, asn1):
        assert asn1 is not None
        try:
            if cls.ASN1SPEC != None:
                value, _ = decoder.decode(asn1, asn1Spec=cls.ASN1SPEC())
            else:
                value = asn1
            kwargs = cls._decode_value(value)
            return cls(**kwargs)
        except PyAsn1Error:
            raise X509DecodeError('Cannot decode general name value')


class X509EmailAddress(X509GeneralName):

    ASN1SPEC = None

    _email = None

    def __init__(self, email):
        self._validate_data(email)
        self._email = email

    def _validate_data(self, data):
        ret = parseaddr(data)
        if ret == ('', ''):
            raise ValueError('Invalid email address %s' % data)

    @classmethod
    def _decode_value(cls, asn1):
        return {'email' : str(asn1)}

    def __unicode__(self):
        return u'email: %s' % self._email


class X509URI(X509GeneralName):

    ASN1SPEC = None

    _uri = None

    def __init__(self, uri):
        self._validate_data(uri)
        self._uri = uri

    def _validate_data(self, data):
        pass

    @classmethod
    def _decode_value(cls, asn1):
        return {'uri' : str(asn1)}

    def __unicode__(self):
        return u'URI: %s' % self._uri


class X509ObjectIdentifier(X509GeneralName):

    ASN1SPEC = None

    _oid = None

    def __init__(self, oid):
        self._validate_data(oid)
        self._oid = oid

    def _validate_data(self, data):
        if not oids.check_oid(data):
            raise ValueError('Invalid OID')

    @classmethod
    def _decode_value(cls, asn1):
        return str(asn1)

    def __unicode__(self):
        return u'OID: %s' % self._oid


class X509IPAddress(X509GeneralName):

    ASN1SPEC = None

    _ip = None

    def __init__(self, ip):
        self._validate_data(ip)
        self._ip = IP(ip)

    def _validate_data(self, data):
        try:
            IP(str(data))
        except ValueError:
            raise ValueError('Invalid IP address %s' % data)

    @classmethod
    def _decode_value(cls, asn1):
        return IP(str(asn1))

    def __unicode__(self):
        return u'IP: %s' % self._data.strNormal()


class X509OtherName(X509GeneralName):

    ASN1SPEC = asn1spec.AnotherName

    _oid = None
    _data = None

    def __init__(self, oid, data=None):
        if not oids.check_oid(oid):
            raise ValueError('Invalid OID')
        self._oid = oid
        self._data = data

    def _validate_data(self, data):
        if not oids.check_oid(data['type-id']):
            raise ValueError('Invalid OID')

    @classmethod
    def _decode_value(cls, asn1):
        return IP(str(asn1))

    def __unicode__(self):
        return u'OID: %s, data %s' % (self._oid, pretty_print_binary(self._data))



class X509DNS(X509GeneralName):

    ASN1SPEC = None

    _dns = None

    def __init__(self, dns):
        self._validate_data(dns)
        self._dns = dns

    def _validate_data(self, data):
        pass

    @classmethod
    def _decode_value(cls, asn1):
        return {'dns' : str(asn1)}

    def __unicode__(self):
        return u'DNS: %s' % self._dns


class X509GeneralNameFactory(object):

    NAMEMAP = {
        'otherName' : X509OtherName,
        'dNSName' : X509DNS,
        #'x400Address' : X509Address,
        'directoryName' : X509DistinguishedName,
        'rfc822Name' : X509EmailAddress,
        #'ediPartyName' : X509EDIPartyName,
        'uniformResourceIdentifier' : X509URI,
        'iPAddress' : X509IPAddress,
        'registeredID' : X509ObjectIdentifier,
        }

    @classmethod
    def from_asn1(cls, asn1):
        assert asn1 is not None
        name = asn1.getName()
        try:
            return cls.NAMEMAP[name].from_asn1(asn1.getComponent())
        except KeyError:
            raise X509DecodeError('Invalid general name type %s' % name)


class X509GeneralNames(object):
    """
    Represents a list of X509 general names (i.e., email addresses, DNs, etc.)
    """

    _names = None

    def __init__(self, names=None):
        self._names = list()
        if names is not None:
            if not isinstance(names, (tuple, list)):
                raise TypeError('Expecting list of general names')
            for element in names:
                if not isinstance(element, (X509GeneralName, X509DistinguishedName)):
                    raise TypeError('Expecting general name instance')
            self._names = names

    def append_general_name(self, name):
        if not isinstance(name, (X509GeneralName, X509DistinguishedName)):
            raise TypeError('Expecting RelativeDistinguishedName instance')
        self._names.append(name)

    def __len__(self):
        return len(self._names)

    def __unicode__(self):
        ret = []
        for name in self._names:
            ret.append(unicode(name))
        return '\n'.join(ret)

    @classmethod
    def from_asn1(cls, asn1):
        assert asn1 is not None
        general_names = cls()
        for name_obj in asn1:
            general_names.append_general_name(X509GeneralNameFactory.from_asn1(name_obj))
        return general_names