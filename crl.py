import oids
import asn1spec
from pyasn1.codec.der import decoder
from pyasn1.codec.ber.decoder import stDumpRawValue
from pyasn1.error import PyAsn1Error

from x509exceptions import X509DecodeError
from generalname import X509GeneralNames
from dirname import X509RelativeDistinguishedName

class X509CRLReason(object):

    REASONTOIDXMAP = {
        'unused' : (0, 'unspecified'),
        'key_compromise' : (1, 'key compromised'),
        'cACompromise' : (2, 'CA compromised'),
        'affiliation_changed' : (3, 'Affiliation changed'),
        'superseded' : (4, 'Superseded'),
        'cessation_of_operation' : (5, 'Cesation of operation'),
        'certificate_hold' : (6, 'Certificate hold'),
        }

    IDXTOREASONMAP = None

    _reasons = None

    def __init__(self, reasons=None):
        if not isinstance(reasons, (tuple, list)):
            raise ValueError('Expecting list of reasons')
        self.IDXTOREASONMAP = dict()
        for name, idx in self.REASONTOIDXMAP.iteritems():
            self.IDXTOREASONMAP[idx[0]] = (name, idx[1])
        self._reasons = [False]*7
        for idx, value in enumerate(reasons):
            self._reasons[idx] = True if value else False

    def set_reason(self, reason, value=False):
        try:
            idx = self.REASONTOIDXMAP[reason]
            self._reasons[idx] = True if value else False
        except KeyError:
            raise ValueError('Unknown reason %s' % reason)

    def __unicode__(self):
        ret = []
        for i in xrange(0, 7):
            if self._reasons[i]:
                ret.append(self.IDXTOREASONMAP[i][1])
        return ', '.join(ret)


class X509CRLDistributionPoint(object):

    ASN1SPEC = asn1spec.DistributionPoint

    _issuer = None
    _dist_point = None
    _relative_name = None
    _reasons = None

    def __init__(self, crl_issuer=None, distribution_point=None, relative_name=None, reasons=None):
        if distribution_point and not isinstance(distribution_point, X509GeneralNames):
            raise ValueError('Expecting general names instance')
        if crl_issuer and not isinstance(crl_issuer, X509GeneralNames):
            raise ValueError('Expecting general names instance')
        if relative_name and not isinstance(relative_name, X509RelativeDistinguishedName):
            raise ValueError('Expecting relative distinguished name instance')
        if reasons and not isinstance(reasons, X509CRLReason):
            raise ValueError('Expecting CRL reason instance')

        if not crl_issuer and not distribution_point and not relative_name:
            raise ValueError('At least CRL issuer or distribution point must be set')
        if distribution_point and relative_name:
            raise ValueError('Either distribution point or relative name must be set, not both')
        self._issuer = crl_issuer
        self._dist_point = distribution_point
        self._relative_name = relative_name
        self._reasons = reasons

    @classmethod
    def from_asn1(cls, asn1):
        if asn1['distributionPoint']:
            relative_name = asn1['distributionPoint']['nameRelativeToCRLIssuer']
            if relative_name:
                relative_name = X509RelativeDistinguishedName.from_asn1(relative_name)
            dist_point = asn1['distributionPoint']['fullName']
            if dist_point:
                dist_point = X509GeneralNames.from_asn1(dist_point)
        reasons = asn1['reasons']
        if reasons:
            reasons = X509CRLReason.from_asn1(reasons)
        return cls(asn1['cRLIssuer'], dist_point, relative_name, reasons)

    def __unicode__(self):
        issuer_str = u'CRL issuer: %s' % unicode(self._issuer) if self._issuer else None
        dist_point_str = u'Distribution point: %s' % unicode(self._dist_point) if self._dist_point else None
        relative_name_str = u'Relative name: %s' % unicode(self._relative_name) if self._relative_name else None
        reason_str = u'Reasons: %s' % unicode(self._reasons) if self._reasons else None
        return '\n'.join(filter(None, [issuer_str, dist_point_str, relative_name_str, reason_str]))


class X509CRLDistributionPoints(object):

    _dist_points = None

    def __init__(self, distribution_points=None):
        self._check_distribution_points(distribution_points)
        self._dist_points = distribution_points or list()

    def _check_distribution_points(self, points):
        if not points:
            return
        for point in points:
            if not isinstance(point, X509CRLDistributionPoint):
                raise ValueError('Expecting list of distribution points')

    def append_distribution_point(self, point):
        if not isinstance(point, X509CRLDistributionPoint):
            raise ValueError('Expecting distribution point instance')
        self._dist_points.append(point)

    @classmethod
    def from_asn1(cls, asn1):
        points = cls()
        for point in asn1:
            points.append_distribution_point(X509CRLDistributionPoint.from_asn1(point))
        return points

    def __unicode__(self):
        return '\n'.join([unicode(point) for point in self._dist_points])
