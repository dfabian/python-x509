import oids
import six
import asn1spec
from pyasn1.codec.der import decoder
from pyasn1.codec.ber.decoder import stDumpRawValue
from pyasn1.error import PyAsn1Error

from x509exceptions import X509DecodeError
import crypto_util
import algorithms

class X509AbstractPublicKey(object):

    ALG_INFO = None
    ALGORITHM = None
    ASN1SPEC = None

    def __init__(self):
        pass

    @classmethod
    def from_asn1(cls, asn1_params):
        assert asn1_params is not None
        try:
            parameters, _ = decoder.decode(asn1_params, asn1Spec=cls.ASN1SPEC())
            return cls(list(parameters))
        except PyAsn1Error:
            raise X509DecodeError('Cannot decode public key parameters')


class X509RSAPublicKey(X509AbstractPublicKey):

    ALG_INFO = algorithms.X509RSAAlgorithmInfo
    ALGORITHM = 'rsa'
    ASN1SPEC = asn1spec.RSAPublicKey

    _modulus = None
    _exponent = None

    def __init__(self, modulus, exponent):
        super(X509RSAPublicKey, self).__init__()
        if not isinstance(modulus, long):
            raise TypeError('Expecting python long for modulus')
        if not isinstance(exponent, (int, long)):
            raise TypeError('Expecting python int/long for exponent')
        self._modulus = modulus
        self._exponent = exponent

    def __unicode__(self):
        return 'RSA public key:\nmodulus: \n%s\nexponent: \n         %d' % (
                crypto_util.pretty_print_binary(str(self._modulus), offset=len('modulus: ')),
                self._exponent)


class X509DSAPublicKey(X509AbstractPublicKey):

    ALG_INFO = algorithms.X509DSAAlgorithmInfo
    ALGORITHM = 'dsa'
    ASN1SPEC = asn1spec.DSAPublicKey

    _p = None
    _q = None
    _g = None
    _y = None

    def __init__(self, y, p, q, g):
        super(X509DSAPublicKey, self).__init__()
        for val in [p, q, g, y]:
            if not isinstance(val, long):
                raise TypeError('Expecting python long for key values')
        self._p = p
        self._q = q
        self._g = g
        self._y = y

    def __unicode__(self):
        return 'DSA public key:\np: %x\n'\
               'q: %x\n'\
               'g: %x\n'\
               'y: %x\n' % (self._p, self._q, self._g, self._y)


class X509PublicKey(object):

    _pubkey = None

    PUBKEY_MAP = {
        X509RSAPublicKey.ALGORITHM : X509RSAPublicKey,
        X509DSAPublicKey.ALGORITHM : X509DSAPublicKey,
        }

    def __init__(self, pubkey):
        if not isinstance(pubkey, X509AbstractPublicKey):
            raise TypeError('Expecting public key instance')
        self._pubkey = pubkey

    def __unicode__(self):
        return unicode(self._pubkey)

    @classmethod
    def from_asn1(cls, asn1):
        alg = algorithms.X509Algorithm.from_asn1(asn1['algorithm']).get_algorithm_info()
        if not alg.is_encryption_algorithm():
            raise X509DecodeError('Expecting encryption algorithm')
        try:
            key_class = cls.PUBKEY_MAP[alg.get_encryption_algorithm()]
        except KeyError:
            raise X509DecodeError('Unknown public key type OID=%s' % alg.OID)

        bytes = crypto_util.bits_to_bytearray(tuple(asn1['subjectPublicKey']))
        pubkey, _ = decoder.decode(str(bytes), asn1Spec=key_class.ASN1SPEC())
        return cls(key_class(*([long(v) for v in list(pubkey)] + alg.get_parameters())))

