from base64 import b64decode

def bits_to_bytearray(bits):
    """
    Transforms a tuple of ones and zeroes to a byte array.
    If length of the bit string length is not multiple of 8, the
    rest is padded with binary zeroes.
    """
    if not isinstance(bits, tuple):
        raise TypeError('Expecting tuple instance')
    partial_bits = len(bits) % 8
    bytes = bytearray()
    for chunk in xrange(0, len(bits), 8):
        byte = 0
        for i in xrange(0, 8):
            if bits[chunk + i] not in (0, 1):
                raise ValueError('Invalid value in bit string')
            byte <<= 1
            byte += bits[chunk + i]
        bytes.append(chr(byte))
    if partial_bits:
        byte = 0
        for i in xrange(partial_bits, 0, -1):
            if bits[-i] not in (0, 1):
                raise ValueError('Invalid value in bit string')
            byte += bits[-i]
            bit <<= 1
        bytes.append(chr(byte))
    return bytes

def pretty_print_binary(data, digets_per_line=18, delimiter=':', offset=0):
    """
    Pretty prints general binary data in hexadecimal. Uses delimiter between byte values.
    The output is formatted to multiple lines, each digets_per_line long. The output can be
    offsetted by offset characters to the right.
    """
    counter = 0
    ret = []
    line = []
    for c in data:
        if counter >= digets_per_line:
            ret.append(line)
            line = []
            counter = 0
        line.append('%02X' % ord(c))
        counter += 1
    return ' '*offset + ('\n' + ' '*offset).join([delimiter.join(line) for line in ret])


def pem_to_der(pem_data, header='-----BEGIN CERTIFICATE-----', footer='-----END CERTIFICATE-----'):
    """
    Converts PEM encoded data to DER encoded data
    """
    start_idx = pem_data.find(header)
    if start_idx == -1:
        return None
    end_idx = pem_data.find(footer, start_idx)
    if end_idx == -1:
        return None
    start_idx += len(header)
    if start_idx > end_idx:
        return None
    stripped = pem_data[start_idx:end_idx]
    joined = ''.join(stripped.strip().replace('\r', '').split())
    return b64decode(joined)