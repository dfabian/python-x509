import six
from pyasn1.error import PyAsn1Error
from pyasn1.codec.der import decoder
from pyasn1.codec.ber.decoder import stDumpRawValue

import datetime
import crypto_util
import asn1spec
import oids
from x509exceptions import X509DecodeError, X509InvalidCertificate
from dirname import X509DistinguishedName
from algorithms import X509Algorithm, X509Signature
from extensions import X509CertificateExtensions
from pubkey import X509PublicKey
from binascii import hexlify
from version import X509Version
from serial import X509SerialNumber

class X509Validity(object):

    not_before = None
    not_after = None

    def __init__(self, before, after):
        if isinstance(before, six.string_types):
            before = self._str_to_datetime(before)
        if isinstance(after, six.string_types):
            after = self._str_to_datetime(after)
        self.not_before = before
        self.not_after = after

    def _str_to_datetime(self, strtime):
        if strtime[-1] != 'Z':
            raise X509DecodeError('Invalid time format %s' % strtime)
        century = ''
        if len(strtime) == 13:
            try:
                year = int(strtime[:2])
            except ValueError:
                raise X509DecodeError('Invalid time format %s' % strtime)
            if 0 <= year < 50:
                century = '20'
            else:
                century = '19'
        try:
            return datetime.datetime.strptime(century + strtime[:-1] + 'GMT', '%Y%m%d%H%M%S%Z')
        except ValueError:
            raise X509DecodeError('Invalid time format %s' % strtime)

    def __unicode__(self):
        return u'not before: %s UTC, not after: %s UTC' % (self.not_before.isoformat(), self.not_after.isoformat())

    @classmethod
    def from_asn1(cls, asn1):
        before = str(asn1['notBefore'].getComponent())
        after = str(asn1['notAfter'].getComponent())
        validity = X509Validity(before, after)
        return validity


class X509Certificate(object):

    version = None
    serial = None
    subject = None
    issuer = None
    signature = None
    validity = None
    extensions = None
    pub_key_info = None

    def __init__(self):
        pass

    def __str__(self):
        return self.__unicode__()

    def __unicode__(self):
        return '\n'.join([
                u'version: ' + unicode(self.version),
                u'serial number: ' + unicode(self.serial),
                u'subject: ' + unicode(self.subject),
                u'issuer: ' + unicode(self.issuer),
                unicode(self.validity),
                unicode(self.pub_key_info),
                unicode(self.extensions),
                unicode(self.signature),
                ])

    @classmethod
    def from_asn1(cls, asn1):
        assert asn1 is not None
        cert = cls()
        if asn1['tbsCertificate'] is None:
            raise X509DecodeError('Certificate has invalid sctructure')
        cert.version = X509Version.from_asn1(asn1['tbsCertificate']['version'])
        cert.serial = X509SerialNumber.from_asn1(asn1['tbsCertificate']['serialNumber'])
        cert.subject = X509DistinguishedName.from_asn1(asn1['tbsCertificate']['subject'])
        cert.issuer = X509DistinguishedName.from_asn1(asn1['tbsCertificate']['issuer'])
        cert.validity = X509Validity.from_asn1(asn1['tbsCertificate']['validity'])
        cert.pub_key_info = X509PublicKey.from_asn1(asn1['tbsCertificate']['subjectPublicKeyInfo'])
        cert.extensions = X509CertificateExtensions.from_asn1(asn1['tbsCertificate']['extensions'])
        cert.signature = X509Signature.from_asn1(asn1['signatureAlgorithm'], asn1['signatureValue'])
        pubkey_alg = X509Algorithm.from_asn1(asn1['tbsCertificate']['signature'])
        if pubkey_alg != cert.signature.get_algorithm():
            raise X509InvalidCertificate('signature algorithms within the certificate do not match')
        return cert

    @classmethod
    def from_binary(cls, der):
        decoder.decode.defaultErrorState = stDumpRawValue
        #try:
        asn1, rest = decoder.decode(der, asn1Spec=asn1spec.Certificate())
        return cls.from_asn1(asn1)
        #except PyAsn1Error as err:
            #raise X509Error('Cannot decode binary certificate')

    @classmethod
    def from_pem(cls, pem):
        der = crypto_util.pem_to_der(pem)
        if not der:
            raise X509DecodeError('Invalid PEM encoded certificate')
        return cls.from_binary(der)