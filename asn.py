from x509 import X509Certificate

import sys
import rsa
#import pkcs1
import time
import multiprocessing

if len(sys.argv) > 1:
    filename = sys.argv[1]
else:
    sys.exit(1)

with open(filename, 'r') as f:
    data = f.read()

if filename.endswith('.crt'):
    cert = X509Certificate.from_binary(data)
else:
    cert = X509Certificate.from_pem(data)
print cert

#start_time = time.time()
#for i in xrange(0, 100):
    #pubkey, privkey = pkcs1.keys.generate_key_pair(size=4096, primality_algorithm='gmpy-miller-rabin')
    #if rsa.prime.randomized_primality_testing(privkey.primes[0], 50) == False:
        #print "not prime"
    #if rsa.prime.randomized_primality_testing(privkey.primes[1], 50) == False:
        #print "not prime"
    #print pubkey
    #print privkey
    #pubkey, privkey = rsa.newkeys(2048, use_gmp=True)
    #print pubkey
    #print privkey
    #if rsa.prime.randomized_primality_testing(privkey.p, 1000) == False:
        #print "not prime"
    #if rsa.prime.randomized_primality_testing(privkey.q, 1000) == False:
        #print "not prime"
#print("--- %s seconds ---" % (time.time() - start_time))
