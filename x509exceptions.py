class X509Error(Exception): pass

class X509DecodeError(X509Error): pass

class X509EncodeError(X509Error): pass

class X509InvalidCertificate(X509Error): pass