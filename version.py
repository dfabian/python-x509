from x509exceptions import X509DecodeError

class X509Version(object):

    X509_CERT_VERSION_MAP = {
        0 : u'v1',
        1 : u'v2',
        2 : u'v3'
        }

    version = None

    def __init__(self, version):
        if version < 0 or version > 2:
            raise ValueError('Invalid version number')
        self.version = version + 1

    @staticmethod
    def from_asn1(asn1):
        assert asn1 is not None
        try:
            version = X509Version(int(asn1))
        except ValueError:
            raise X509DecodeError('Invalid version number')
        return version

    def __unicode__(self):
        return u'%u (%0u)' % (self.version, self.version - 1)